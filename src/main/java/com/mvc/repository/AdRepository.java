package com.mvc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mvc.entity.Ad;

@Repository
public interface AdRepository extends JpaRepository<Ad, Long>{


}
