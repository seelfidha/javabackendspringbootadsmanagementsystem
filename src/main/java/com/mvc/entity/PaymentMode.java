package com.mvc.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class PaymentMode {

	@Id
	@GeneratedValue
	private long idPayment;
	
	private String label;
	
	private String description;
	
	@OneToMany(mappedBy="paymentMode")
	private List<Ad> ads;
}
