package com.mvc.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.mvc.entity.User;
import com.mvc.repository.UserRepository;

@Service
@Transactional
public class UserService {
	
	@Autowired
	private UserRepository userRepository;

	
	public List<User>findAll(){
		return this.userRepository.findAll();
	}
	
	public Optional<User>findUserByID(Long idUser){
		return userRepository.findById(idUser);
	}
	
	public User save(User user){
		return this.userRepository.save(user);
	}
	
	public void deleteByID(Long idUser){
		this.userRepository.deleteById(idUser);
	}
}
