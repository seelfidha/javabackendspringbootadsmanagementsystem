package com.mvc.service;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mvc.entity.Category;
import com.mvc.repository.CategoryRepository;

@Service
@Transactional
public class CategoryService {

	@Autowired
	private CategoryRepository categoryRepository;
	
	public List<Category>findAll(){
		return this.categoryRepository.findAll();
	}
	
	public Optional<Category> findByID(Long idCategory){
		return this.categoryRepository.findById(idCategory);
	}
	
	public Category save(Category category){
		
		return categoryRepository.save(category);
	}
	
	public void deleteByID(Long idCategory){
		this.categoryRepository.deleteById(idCategory);
	}
}
