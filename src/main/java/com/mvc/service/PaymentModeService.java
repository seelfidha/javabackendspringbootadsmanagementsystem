package com.mvc.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mvc.entity.PaymentMode;
import com.mvc.repository.PaymentModeRepository;

@Service
@Transactional
public class PaymentModeService {

	@Autowired
	private PaymentModeRepository paymentModeRepository;
	
	
	public List<PaymentMode>findAll(){
		return this.paymentModeRepository.findAll();
	}
	
	public Optional<PaymentMode> findByID(Long idPaymentMode){
		return this.paymentModeRepository.findById(idPaymentMode);
	}
	
	public PaymentMode save(PaymentMode paymentMode){
		
		return paymentModeRepository.save(paymentMode);
	}
	
	public void deleteByID(Long idPaymentMode){
		this.paymentModeRepository.deleteById(idPaymentMode);
	}
}
