package com.mvc.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.mvc.entity.Ad;
import com.mvc.repository.AdRepository;


@Service
@Transactional
public class AdService {

	@Autowired
	private AdRepository adRepository;
	
	@GetMapping("/all")
	public List<Ad>findAll(){
		return this.adRepository.findAll();
	}
	
	@GetMapping("/one/{idAd}")
	public Optional<Ad>findAdByID(@PathVariable Long idAd){
		return this.adRepository.findById(idAd);
	}
	
	@PostMapping("/save")
	public Ad save(@RequestBody Ad ad){
		return this.adRepository.save(ad);
	}
	
	@PutMapping("/update")
	public Ad update(@RequestBody Ad ad){
		return this.adRepository.save(ad);
	}
	
	@DeleteMapping("/delete/{idAd}")
	public void deleteAdByID(@PathVariable Long idAd){
		this.adRepository.deleteById(idAd);
	}
}
