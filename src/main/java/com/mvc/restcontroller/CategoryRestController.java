package com.mvc.restcontroller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mvc.entity.Category;
import com.mvc.service.CategoryService;

@RestController
@RequestMapping("/api/categories")
@CrossOrigin(origins="*",allowedHeaders ="*")
public class CategoryRestController {

	@Autowired
	private CategoryService categoryService;
	
	@GetMapping("/all")
	public List<Category>findAll(){
		return this.categoryService.findAll();
	}
	
	@GetMapping("/one/{idCategory}")
	public Optional<Category> findByID(@PathVariable Long idCategory){
		return this.categoryService.findByID(idCategory);
	}
	
	@PostMapping("/save")
	public Category saveCategory(@RequestBody Category category){
		return this.categoryService.save(category);
	}
	
	@PutMapping("/update")
	public Category updateCategory(@RequestBody Category category){
		return this.categoryService.save(category);
	}
	
	@DeleteMapping("/delete/{idCategory}")
	public void deleteCategory(@PathVariable Long idCategory){
		this.categoryService.deleteByID(idCategory);
	}
}
