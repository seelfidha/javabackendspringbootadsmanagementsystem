package com.mvc.restcontroller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mvc.entity.PaymentMode;
import com.mvc.service.PaymentModeService;

@RestController
@RequestMapping("/api/paymentModes")
@CrossOrigin(origins="*", allowedHeaders="*")
public class PaymentModeRestController {

	@Autowired
	private PaymentModeService paymentModeService;
	
	@GetMapping("/all")
	public List<PaymentMode>findAll(){
		return this.paymentModeService.findAll();
	}
	
	@GetMapping("/one/{idPaymentMode}")
	public Optional<PaymentMode> findByID(@PathVariable Long idPaymentMode){
		return this.paymentModeService.findByID(idPaymentMode);
	}
	
	@PostMapping("/save")
	public PaymentMode save(@RequestBody PaymentMode paymentMode){
		return this.paymentModeService.save(paymentMode);
	}
	
	@PutMapping("/update")
	public PaymentMode update(@RequestBody PaymentMode paymentMode){
		return this.paymentModeService.save(paymentMode);
	}
	
	@DeleteMapping("/delete/{idPaymentMode}")
	public void deletePaymentByID(@PathVariable Long idPaymentMode){
		this.paymentModeService.deleteByID(idPaymentMode);
	}
}
