package com.mvc.restcontroller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mvc.entity.User;
import com.mvc.service.UserService;

@RestController
@RequestMapping("/api/users")
@CrossOrigin(origins="*", allowedHeaders="*")
public class UserRestController {

	@Autowired
	private UserService userService;
	
	@GetMapping("/all")
	public List<User>findAll(){
		return this.userService.findAll();
	}
	
	@GetMapping("/one/{idUser}")
	public Optional<User>findUserByID(@PathVariable Long idUser){
		return this.userService.findUserByID(idUser);
	}
	
	@PostMapping("/save")
	public User saveUser(@RequestBody User user){
		return this.userService.save(user);
	}
	
	@PutMapping("/update")
	public User updateUser(@RequestBody User user){
		return this.userService.save(user);
	}
	
	@DeleteMapping("/delete/{idUser}")
	public void deleteUser(@PathVariable Long idUser){
		this.userService.deleteByID(idUser);
	}
}
