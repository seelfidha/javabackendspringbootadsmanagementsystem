package com.mvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootAdsManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootAdsManagementApplication.class, args);
	}

}
